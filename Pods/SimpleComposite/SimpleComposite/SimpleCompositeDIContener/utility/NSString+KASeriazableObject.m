//
//  NSString.m
//  kakebo
//
//  Created by Didier Lobeau on 02/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "NSString+KASeriazableObject.h"

@implementation NSString (KASeriazableObject)

static NSString * PATH_SEPARATOR = @"/";
static NSString * FUNCTION_PARAMETER_SEPARATOR = @":";
static NSString * FUNCTION_PARAMETER_MARKER = @"|";

+(NSString *) pathSeparator
{
    return PATH_SEPARATOR;
}

+(NSString *) pathTag
{
    return @"path";
}

+(NSString *) objectFamilyNameTag
{
    return @"objectFamilyName";
}

-(NSString *) fileNameFromPath
{
    NSString *ReturnValue = nil;
    
    NSArray * ListString = [self componentsSeparatedByString:@":"];
    if([ListString count]> 0 )
    {
        ReturnValue = [ListString objectAtIndex:0];
    }
    return ReturnValue;
}

-(NSString *) lastElementFromPath
{
    NSString *ReturnValue = nil;
    
    NSArray * ListString = [self nodeIDList];
    
    NSInteger count = [ListString count];
    if(count > 0)
    {
        ReturnValue = [ListString objectAtIndex:(count -1)];
    }
    
    return ReturnValue;
}

-(NSString *) firstElementFromPath
{
    NSString *ReturnValue = nil;
    
    NSArray * ListString = [self nodeIDList];
    
    NSInteger count = [ListString count];
    if(count > 1)
    {
        ReturnValue = [ListString objectAtIndex:1];
    }
    
    return ReturnValue;
}

-(NSArray<NSString *>*) nodeIDList
{
    NSArray<NSString *>*ReturnValue = nil;
    @synchronized(self)
    {
        ReturnValue = [self componentsSeparatedByString:PATH_SEPARATOR];
    }
        
    return ReturnValue;
}

-(BOOL) isObjectFamilyReference
{
    BOOL ReturnValue = false;
    if([self length] > 0)
    {
        char first = [self characterAtIndex:0];
        char last = [self characterAtIndex:([self length] - 1)];
    
        ReturnValue = ((first == '[') && (last == ']'));
    }
    return ReturnValue;
}

-(NSString *) extractObjectFamilyMarkers
{
    NSString * ReturnValue = [self copy];
    
    if([self isObjectFamilyReference])
    {
        NSCharacterSet *charactersToRemove = [NSCharacterSet characterSetWithCharactersInString:@"[]"];
        ReturnValue = [ReturnValue stringByTrimmingCharactersInSet:charactersToRemove];
    }
    
    return ReturnValue;
}

-(NSString *) extractObjectFunctionParametersMarkers
{
    NSString * ReturnValue = [self copy];
    
     NSCharacterSet *charactersToRemove = [NSCharacterSet characterSetWithCharactersInString:@"()"];
    ReturnValue = [ReturnValue stringByTrimmingCharactersInSet:charactersToRemove];
    
    
    return ReturnValue;
}

-(NSDictionary<NSString *,NSString *> *) splitWithPathAndFamillyName
{
    NSMutableDictionary<NSString *,NSString *> *ReturnValue = nil;
    
    NSArray<NSString *>* List = [self nodeIDList];
    
    if([List count] >1)
    {
        ReturnValue = [[NSMutableDictionary alloc] init];
        NSString * FamilyNAme = [List objectAtIndex:([List count]-1)];
        [ReturnValue setObject:FamilyNAme forKey:[NSString objectFamilyNameTag]];
        
        NSString *  Path  = [self substringToIndex:([self length]-[FamilyNAme length]-1)];
    [ReturnValue setObject:Path forKey:[NSString pathTag]];

    }
    return ReturnValue;
}


-(BOOL) isPathRoot
{
    return ([self length] == 0);
}

-(NSString *) nextPathBranch
{
    
    NSArray<NSString *> *ListBranch = [self nodeIDList];
    
    NSString *currentBranch = [ListBranch objectAtIndex:0];
    
    
    if([currentBranch isPathRoot]
       && ([ListBranch count]>0))
    {
        currentBranch = [NSString stringWithFormat:@"%@%@",[NSString pathSeparator],[ListBranch objectAtIndex:1]];
        
    }
    NSAssert([currentBranch length]+1<[self length], @"%@ not include in %@",currentBranch,self);
    
    return [self substringFromIndex:([currentBranch length]+1)];
    
}

-(NSNumber *) toNumber
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    return  [f numberFromString:self];
}


@end
