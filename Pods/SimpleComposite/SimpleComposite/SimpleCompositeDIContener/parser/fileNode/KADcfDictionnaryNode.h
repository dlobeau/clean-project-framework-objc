//
//  BVDictionnaryNode.h
//  Skwipy
//
//  Created by Didier Lobeau on 04/02/2016.
//  Copyright (c) 2016 FollowTheDancer. All rights reserved.
//

#import "KADcfGenericNode.h"

@interface KADcfDictionnaryNode : KADcfGenericNode






-(id) initWithDictionary:(NSDictionary *) Dictionary  WithNodeName:(NSString *)NodeName;


@end
