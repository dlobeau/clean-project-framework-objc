//
//  DcfParser.h
//  kakebo
//
//  Created by Didier Lobeau on 31/05/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>


//#import "KADcfDom.h"
@protocol KAError;

@protocol KASeriazableObject;
@protocol KASeriazableObjectTable;
@protocol KASerializeObjectFactory;

@protocol KAAttributeStreamParser <NSObject>

-(id<KASeriazableObjectTable>) parseWithFactory:(id<KASerializeObjectFactory>)factory;



@end
