//
//  KAAttributeStreamXMLParser.m
//  kakebo
//
//  Created by Didier Lobeau on 08/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KAAttributeStreamXMLParser.h"
#import "NSDictionary+KASeriazableObject.h"


@interface KAAttributeStreamXMLParser()


@end

@implementation KAAttributeStreamXMLParser



-(id) initWithString:(NSString *) StringToParse
{
    self = [super init];
    self.stringToBeParsed = StringToParse;
    return self;
}



-(id<KASeriazableObject>) parseWithFactory:(id<KASerializeObjectFactory>)factory
{
    id<KASeriazableObject> ReturnValue = nil;
    
    NSDictionary *plist = [NSDictionary dictionaryFromString:self.stringToBeParsed];
         
    if(plist != nil)
    {
        ReturnValue = [plist getSeriazableObjectWithFactory:factory];
    }
  
    return ReturnValue;
}



@end
