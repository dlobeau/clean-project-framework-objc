//
//  KAAGenericAttributeFactory.m
//  kakebo
//
//  Created by Didier Lobeau on 13/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASerializeObjectFactoryImp.h"
#import "KASeriazableObjectImp.h"
#import "KAFile.h"
#import "KASerializeObjectLink.h"
#import "KAAttributeStreamXMLParser.h"
#import "KAAttributeStreamParserPlistFile.h"
#import "KABasicFile.h"
#import "KADataBaseFile.h"
#import "KAGenericObjCInjectionDependencyDelegate.h"

@interface KASerializeObjectFactoryImp()


@property NSBundle *bundle;
@property id<KAInjectionDependencyDelegate> DIDelegate;
@end


@implementation KASerializeObjectFactoryImp



static NSString * LINK_TAG = @"Link";

 -(id) initWithWithBundle:(NSBundle *) bundle withDIDelegate:(id<KAInjectionDependencyDelegate>) DIDelegate
{
    self = [super init];
    self.bundle = bundle;
    self.DIDelegate = DIDelegate;
    return self;
}


-(id<KASeriazableObject>) createAttributeFromLabel:(NSString *) label
                                              WithID:(NSString *) ID
                                         WithGRoupId:(NSString *) GroupId
                                          WithTypeId:(NSString * ) TypeId
{
    id<KASeriazableObject> ReturnValue = nil;
    NSMutableDictionary<NSString *,NSString *> *Dictionany = [[NSMutableDictionary alloc] init];
    
    [Dictionany setObject:[label copy] forKey:[KASeriazableObjectImp getLabelTag]];
    [Dictionany setObject:[ID copy] forKey:[KASeriazableObjectImp getLabeLIdentifierTag]];
    [Dictionany setObject:[GroupId copy] forKey:[KASeriazableObjectImp getGroupIdTag]];
    [Dictionany setObject:[TypeId copy] forKey:[KASeriazableObjectImp getTypeIdTag]];
    ReturnValue = [self createAttributeFromDictionary:Dictionany];
    ReturnValue.factoryDelegate = self;
    return ReturnValue;
}




-(id<KASeriazableObject>) createAttributeFromDictionary:(NSDictionary *) AttributeDictionary
{
    id<KASeriazableObject> ReturnValue;
   
    ReturnValue = [self.DIDelegate injectedObjectWithParameters:AttributeDictionary];
        
    ReturnValue.factoryDelegate = self;
    
    return ReturnValue;
}

-(id<KASeriazableObject>) createObjectFromFamilyType:(NSString *)FamilyType
{
    id<KASeriazableObject> ReturnValue = [self createAttributeFromLabel:FamilyType WithID:FamilyType
                                                            WithGRoupId:FamilyType WithTypeId:FamilyType];
   
    return ReturnValue;
    
}



-(id<KASeriazableObjectTable>) createAttributeFromString:(NSString *) SerializedAttribute
{
    KAAttributeStreamXMLParser *parser = [[KAAttributeStreamXMLParser alloc] initWithString:SerializedAttribute];
    
    return [parser parseWithFactory:self];
}

-(id<KASerializeObjectLink>) createFromSource:(id<KASeriazableObject>) Source WithID:(NSString *) ID
{
    id<KASerializeObjectLink> ReturnValue = nil;
    
    
    if([Source conformsToProtocol:@protocol(KASerializeObjectLink) ])
    {
        ReturnValue = (id<KASerializeObjectLink>)Source.cloneObject;
    }
    else
    {
        NSString *LinkCompletePath = [Source path];
        
        
        ReturnValue =( id<KASerializeObjectLink>)[self createAttributeFromLabel:LinkCompletePath WithID:LinkCompletePath WithGRoupId:LINK_TAG WithTypeId:ID ];
        
        [ReturnValue setSource: Source];
    
    }
    
    return ReturnValue;
}

-(id) createWithContentsOfFile:(id<KAFile>)File
{
    NSAssert([File isFileExist], @"File: %@ is not found", [File fileName]);
    
    id<KASeriazableObjectTable> ReturnValue = nil;
    
    KAAttributeStreamParserPlistFile *parser = [[KAAttributeStreamParserPlistFile alloc] initWithFile:File];
    
    ReturnValue = [parser parseWithFactory:self];
    
    return ReturnValue;
}

- (id)createWithContentsOfFileWithFileName:(NSString *)FileName
{
    id<KAFile> File = [[KABasicFile alloc] initWithFileName:FileName WithBundle:self.bundle] ;
    
    id<KASeriazableObjectTable> ReturnValue = [self createWithContentsOfFile:File];
    
    return ReturnValue;
}

- (id<KAFile>)createFileWithFileName:(NSString *)FileName
{
    id<KAFile> File = [[KABasicFile alloc] initWithFileName:FileName WithBundle:self.bundle] ;
    
    return File;
}

- (id<KAFile>)createDataBaseFileWithFileName:(NSString *)FileName
{
    id<KAFile> File = [[KADataBaseFile alloc] initWithFileName:FileName WithBundle:self.bundle] ;
    
    return File;
}

- (id)createWithContentsOfDataBaseFileWithFileName:(NSString *)FileName
{
    id<KAFile> File = [[KADataBaseFile alloc] initWithFileName:FileName WithBundle:self.bundle] ;
    
    id<KASeriazableObjectTable> ReturnValue = [self createWithContentsOfFile:File];
    
    return ReturnValue;
}

-(NSDictionary<NSString *,NSString *> *) injectionFileContentWithFileName:(NSString *)FileName
{
    id<KAFile> F = [self createFileWithFileName:FileName];
    NSDictionary * DictinonaryFromFile = [[NSDictionary alloc] initWithContentsOfFile:F.getFileCompleteName];
    
    return DictinonaryFromFile;
}

@end
