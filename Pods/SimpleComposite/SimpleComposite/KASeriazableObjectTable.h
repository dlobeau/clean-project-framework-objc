//
//  KAAttributeContener.h
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KASeriazableObject.h"

@protocol KATableChildsManagementDelegate;

@protocol KASeriazableObjectTable <KASeriazableObject>

-(id<KASeriazableObject>) objectAtIndex:(NSInteger) Index;

-(void) removeObjectAtIndex:(NSInteger) Index;

-(NSUInteger) count;
-(NSInteger) foundWithTypeId:(NSString *) TypeId;

-(BOOL) changeChildAtRank:(NSInteger) rank WithValue:(id<KASeriazableObject>) Value;




-(void) setSelfAsParentWithObject:(id<KASeriazableObject>) Object;




-(BOOL) isInsertionOkForChild:(id<KASeriazableObject>) Child;



-(id<KASeriazableObjectTable>)cloneObject;

-(id<KASeriazableObject>) scanChildsForItemWithID:(NSString *) ID  WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude;

-(id<KATableChildsManagementDelegate>) childsDelegate;
-(void) setChildsDelegate:(id<KATableChildsManagementDelegate>) childsDelegate;


@end
