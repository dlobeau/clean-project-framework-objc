//
//  KATableFifoChildsManagement.m
//  kakebo
//
//  Created by Didier Lobeau on 10/04/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KATableFifoChildsManagement.h"
#import "KASeriazableObjectTable.h"

@interface KATableFifoChildsManagement()

@property NSMutableArray<id<KASeriazableObject>> * attributes;
@property id<KASeriazableObjectTable> sender;
@end

@implementation KATableFifoChildsManagement


@synthesize attributes = _attributes;



-(NSMutableArray<id<KASeriazableObject>> *) attributes
{
    NSMutableArray<id<KASeriazableObject>> *ReturnValue = nil;
    @synchronized (self.sender)
    {
        if(_attributes == nil)
        {
           _attributes = [[NSMutableArray<id<KASeriazableObject>> alloc] init];
           
        }
         ReturnValue = _attributes;
    }
   
    return ReturnValue;
}
-(void) setAttributes:(NSMutableArray<id<KASeriazableObject>> *)attributes
{
    @synchronized (self.sender)
    {
        _attributes = attributes;
    }
}

-(id) initWithSender:(id<KASeriazableObjectTable>) Sender
{
    self = [super init];
    self.sender = Sender;
    return self;
}

- (NSUInteger) count
{
    return [self.attributes count];
}

-(void) addInChildContenerObject:(id<KASeriazableObject>) Object
{
     @synchronized(self.attributes) {
         
        [self.sender setSelfAsParentWithObject:Object];
        [self.attributes addObject:Object ];
    }
}

-(BOOL) emptyChildList
{
    [self.attributes removeAllObjects];
    return YES;
}

-(BOOL) removeChild:(id<KASeriazableObject> ) ChildObject
{
    BOOL bReturn = NO;
    
    @synchronized(self.sender)
    {
       [(NSMutableOrderedSet *)self.attributes removeObject:ChildObject];
    
    
        [ChildObject setParent:nil];
        [ChildObject setIsDeleted:YES];
        bReturn = YES;
    }
    return bReturn;
}

-(void) removeObjectAtIndex:(NSInteger) Index
{
    @synchronized(self.sender) {
        [self.attributes removeObjectAtIndex:Index];
    }
    
}

-(NSArray<id<KASeriazableObject>> *) childs
{
    return [self attributes];
}

-(BOOL) changeChildAtRank:(NSInteger) rank WithValue:(id<KASeriazableObject>) Value
{
    BOOL bReturnValue = YES;
     @synchronized(self.sender)
    {
        if(rank >= [self count])
        {
            [self addInChildContenerObject:Value];
            bReturnValue = YES;
        }
        else
        {
           
               [self.attributes replaceObjectAtIndex:rank withObject:Value ];
           
            
            [self.sender setSelfAsParentWithObject:Value];
            
        }
    }
    return YES;
}

-(id<KAIterator>) getIterator
{
    return [[KAAttributeContenerIterator alloc] initWithContener:self.attributes];
    
}

-(NSInteger) rankWithKey:(id<KASeriazableObject>)key
{
    //Binary search in oredered tree: Algorithms, 4th ed. - [Sedgewick, Wayne] Page 380
    NSInteger ReturnValueAsLow = 0;
    @synchronized (self.sender)
    {
        
        BOOL bExit = NO;
        NSInteger  hi = [self count]-1;
        
        id<KASeriazableObjectTable> keysContener = self.sender;
        while ((ReturnValueAsLow <= hi) && !bExit)
        {
            NSInteger mid = ReturnValueAsLow + (hi - ReturnValueAsLow) / 2;
            NSInteger cmp = [key compareAttribute:[keysContener objectAtIndex:mid]];
            if (cmp < 0)
            {
                hi = mid - 1;
            }
            else if (cmp > 0)
            {
                ReturnValueAsLow = mid + 1;
            }
            else
            {
                ReturnValueAsLow = mid;
                bExit = YES;
            }
        }
    }
    return ReturnValueAsLow;
}



-(id<KASeriazableObjectTable>) getValueWithKey:(id<KASeriazableObject> )Key WithKEyContener:(nonnull id<KASeriazableObjectTable>)keysContener
{
    id<KASeriazableObjectTable> ReturnValue = nil;
    @synchronized (self.sender)
    {
        if ([keysContener count] > 0)
        {
            NSInteger i = [keysContener.childsDelegate rankWithKey:Key];
            if (i < [keysContener count] &&
                [[keysContener objectAtIndex:i] compareAttribute:Key] == NSOrderedSame)
            {
                ReturnValue = (id<KASeriazableObjectTable>)[self.sender objectAtIndex:i];
            }
            
        }
    }
    return ReturnValue;
}

-(void) removeKey:(id<KASeriazableObject>) Key withRank:(NSInteger) Rank
{
    @synchronized (self.sender)
    {
        NSInteger Size = [self count];
      
        if( Rank< Size )
        {
            if(Rank == (Size -1))
            {
                [self.sender removeObjectAtIndex:Rank];
            }
            else
            {
                for (NSInteger j = Rank; j < (Size -1); j++)
                {
                    [self.sender changeChildAtRank:j WithValue:[self.sender objectAtIndex:j+1]];
                    
                }
                [self.sender removeObjectAtIndex:(Size-1)];
            }
        }
    }
}


@end

@interface KAAttributeContenerIterator()


@property NSInteger limitIndex ;
@property NSArray<id<KASeriazableObject>> *list;
@property NSInteger size;
@property NSInteger index;
@property NSArray<id<KASeriazableObject>> * additionalDynamicAttributes;

@property BOOL isParsingAdditionalContener;
@end



@implementation KAAttributeContenerIterator
@synthesize size = _size;
@synthesize index = _index;

-(BOOL) hasNext
{
    return (self.index < self.size);
}
-(id<KASeriazableObject>) next
{
     id<KASeriazableObject> ReturnValue = nil;
    @synchronized(self) {
        
        
         ReturnValue = [self.list objectAtIndex:self.index];
        self.index++;
        
        if(![self hasNext] && !self.isParsingAdditionalContener
           && (self.additionalDynamicAttributes != nil) )
        {
            [self setWithList:self.additionalDynamicAttributes];
            self.isParsingAdditionalContener = YES;
        }
    }
    return ReturnValue;
}

-(NSInteger) size
{
    return _size;
}

-(NSInteger) index
{
    return _index;
}
-(void) setIndex:(NSInteger)index
{
    _index = index;
}

-(void) setSize:(NSInteger)size
{
    _size = size;
}

-(id) initWithContener:(NSArray *) Contener
{
    self = [super init];
    [self setWithList:Contener];
    self.isParsingAdditionalContener = NO;
    self.additionalDynamicAttributes = nil;
    return self;
}

-(id) initWithContener:(NSArray *) Contener WithAdditionalData:(NSArray<id<KASeriazableObject>> * ) List
{
    self = [super init];
    [self setWithList:Contener];
    self.isParsingAdditionalContener = NO;
    self.additionalDynamicAttributes = List;
    return self;
}



-(void) setWithList:(NSArray<id<KASeriazableObject>> * ) List
{
    self.index = 0;
    self.size = [List count];
    self.list = List ;
}

@end
