//
//  KAGenericAttribute.m
//  kakebo
//
//  Created by Didier Lobeau on 10/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectImp.h"
#import "KASeriazableObjectTableImp.h"
#import "KASerializeObjectFactoryImp.h"

#import "KABasicFile.h"
#import "NSDictionary+KASeriazableObject.h"
#import "NSString+KASeriazableObject.h"
#import "KASerializableObjectToFile.h"
#import "KASerializeObjectFactory.h"

@interface KASeriazableObjectImp()

    @property NSString * label;
    @property NSString * labelIdentifier;
    @property NSString * objectFamilyName;
    @property NSString * ID;
    @property  id<KASerializeObjectFactory> factoryDelegate;
    @property NSString *path;
   @property  id<KASeriazableObject> parent;
   

@end

@implementation KASeriazableObjectImp

static NSString * TYPE_ID = @"type_id";
static NSString * LABEL_IDENTIFIER = @"identifier";
static NSString * GROUP_ID = @"group_id";
static NSString * LABEL = @"label";
static NSString * NODE_CHILDS_TAG = @"childs";


@synthesize label = _label;
@synthesize labelIdentifier = _labelIdentifier;
@synthesize objectFamilyName = _objectFamilyName;
@synthesize ID = _ID;
@synthesize parent = _parent;
@synthesize path = _path;



static NSArray *TagArrayList = nil;

+(NSString *) getTypeTag
{
    return @"serilizableObjectEntity";
}

+(NSString *) getTypeIdTag
{
    return TYPE_ID;
}
+(NSString *) getLabeLIdentifierTag
{
    return LABEL_IDENTIFIER;
}
+(NSString *) getGroupIdTag
{
    return GROUP_ID;
}
+(NSString *) getLabelTag
{
    return LABEL;
}


+(BOOL) checkDictionnary:(NSDictionary<NSString *,NSString *> *)Dictionnary
{
    BOOL bReturnValue = NO;
    
    NSString * type_id = [Dictionnary objectForKey:TYPE_ID];
    NSString * identifier = [Dictionnary objectForKey:LABEL_IDENTIFIER];
    NSString * group_id = [Dictionnary objectForKey:GROUP_ID];
    NSString * label = [Dictionnary objectForKey:LABEL];
    
    if( (type_id != nil )&& ( identifier != nil ) && ( group_id != nil ) && ( label != nil ) )
    {
        bReturnValue = YES;
    }
    return bReturnValue;
}

-(id) initWithDictionary:(NSDictionary *) Dictionary
{
    
    NSString * type_id = [Dictionary objectForKey:TYPE_ID];
    NSString * identifier = [Dictionary objectForKey:LABEL_IDENTIFIER];
    NSString * group_id = [Dictionary objectForKey:GROUP_ID];
    NSString * label = [Dictionary objectForKey:LABEL];
    
    NSAssert([KASeriazableObjectImp checkDictionnary:Dictionary], @"check dictionnary structure: %@",Dictionary);
    
    return [self initWithLabel:label WithLabelIdentifier:identifier WithObjectFamilyName:group_id WithID:type_id ];;
}

-(id) initWithLabel:(NSString *) label
                          WithLabelIdentifier:(NSString *) ID
                     WithObjectFamilyName:(NSString *) GroupId
                      WithID:(NSString * ) TypeId

{
    self = [super init];
   
    self.label = [label copy];
    self.labelIdentifier = [ID copy];
    self.objectFamilyName = [GroupId copy];
    self.ID = [TypeId copy];
    return self;
}


-(id<KASeriazableObject>) root
{
    id<KASeriazableObject,KATreeNavigable> ReturnValue = nil;
    
    ReturnValue = self;
    
    while(ReturnValue.parent != nil)
    {
        ReturnValue = (id<KASeriazableObject,KATreeNavigable>)ReturnValue.parent;
    }
    
    
    
    return ReturnValue;
}



-(id<KASeriazableObject>) parent
{
    return _parent;
}
-(void) setParent:(id<KASeriazableObject>)Parent
{
    if(Parent != nil)
    {
        [self setIsDeleted:NO];
    }
    NSAssert(Parent !=self, @"parent must be different then self, check %@",self.attributeIdentity );
    _parent = Parent;
}

-(NSArray<id<KASeriazableObject>> *) foundValueInTree:(id<KASeriazableObject>) Key
{
    return nil;
}



- (NSComparisonResult ) compareAttribute:(id<KASeriazableObject> ) AttributeToBeCompared
{
    NSComparisonResult ReturnValue = NSOrderedAscending;
    if( AttributeToBeCompared != nil)
    {
        if( [self.objectFamilyName isEqual:AttributeToBeCompared.objectFamilyName])
        {
            ReturnValue = [self.labelIdentifier compare:AttributeToBeCompared.labelIdentifier];
        }
        else
        {
            ReturnValue = NSOrderedAscending;
        }
        
    }
    return ReturnValue;
}

- (id<KASeriazableObject>) cloneObject
{
    id<KASeriazableObject> ReturnValue = [self cloneObjectWithoutChild];
    
    id<KAIterator> Iterator = [self getIterator];
    while([Iterator hasNext])
    {
        id<KASeriazableObject> currentChild = [Iterator next];
        id<KASeriazableObject> Clone = [currentChild cloneObject];
        Clone.ID = currentChild.ID;
        [ReturnValue addChild:Clone];
    }
    [ReturnValue setIsSerializable:self.isSerializable];
    return ReturnValue;
    
}
- (id<KASeriazableObject>) cloneObjectWithoutChild
{
    id<KASeriazableObject> ReturnValue = nil;
    
    NSAssert(self.factoryDelegate!= nil, @"factory should be defined for object, check %@",self.attributeIdentity);
   
    ReturnValue  = [self.factoryDelegate createAttributeFromLabel:[_label copy] WithID:[_labelIdentifier copy] WithGRoupId:[_objectFamilyName copy] WithTypeId:[_ID copy] ];
    
    return ReturnValue;
}

-(BOOL) emptyChildList
{
    return NO;
}



-(NSString *) attributeIdentity
{
    NSString *ReturnValue = nil;
    
    ReturnValue = [NSString stringWithFormat:@"group_id: %@, path: %@",self.objectFamilyName,self.path];
    
    return ReturnValue;
}



-(id<KAIterator>) getIterator
{
    return nil;
}

-(id<KAIterator>) getIteratorWithDynamicChildsWithID:(NSString *) ID
{
    return nil;
}



-(BOOL) addChild:(id<KASeriazableObject> ) ChildObject
{
    return NO;
}

-(BOOL) addChild:(KASeriazableObjectImp * ) ChildObject WithTypeId:(NSString *) TypeId
{
    return NO;
}

-(BOOL) addCloneOfChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id
{
    return NO;
}
-(BOOL) addLinkToChild:(id<KASeriazableObject> ) ChildObject WithTypeId:(NSString *) Type_id
{
    return NO;
}
-(BOOL) addAllChild:(NSArray<id<KASeriazableObject>> *) ChildListObject
{
    return NO;
}


                      

-(NSString *) path
{
    if(_path == nil)
    {
        _path =self.refreshedPath;
    }
    return _path;
    
}
-(void) setPath:(NSString *)path
{
    _path = path;
}

- (NSString *) refreshedPath
{
    NSString *ReturnValue = nil;
    
    id<KASeriazableObject> currentNode = self;
    
    ReturnValue = [currentNode ID];
    
    currentNode = [self parent];
    
    while(currentNode != nil)
    {
        ReturnValue = [NSString stringWithFormat:@"%@%@%@",[currentNode ID],[NSString pathSeparator],ReturnValue] ;
        currentNode = [currentNode parent];
    }
    ReturnValue = [NSString stringWithFormat:@"%@%@",[NSString pathSeparator],ReturnValue];
    
    return ReturnValue;
}

-(BOOL) isParentOfChild:(id<KASeriazableObject>) Child
{
    BOOL ReturnValue = NO;
    
    id<KASeriazableObject> Parent = Child.parent;
    
    if(Parent == self)
    {
        ReturnValue = YES;
    }
    
    while((Parent != nil) && (!ReturnValue))
    {
        Parent = Parent.parent;
        
        if(Parent == self)
        {
            ReturnValue = YES;
        }
    }
    
    return ReturnValue;
}

-(BOOL) removeChild:(id<KASeriazableObject> ) ChildObject
{
    return NO;
}
-(BOOL) removeAllChild:(NSArray<id<KASeriazableObject>>* ) ChildObjectList
{
    return NO;
}

-(id<KASeriazableObject> ) getChildwithTypeId:(NSString *) TypeId
{
    return nil;
}
-(id<KASeriazableObject> ) getChildwithTypeId:(NSString *) TypeId WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude
{
    return nil;
}
-(id<KASeriazableObject> ) getChildwithIdentifier:(NSString *) Identifier
{
    return nil;
}
-(NSArray<id<KASeriazableObject>> *) getChildListWithGroupId:(NSString *) GroupId
{
    return nil;
}
-(NSArray<id<KASeriazableObject>> *) getChildListWithGroupId:(NSString *) GroupId WithCompliantKey:(id<KASeriazableObject>)Key
{
    return nil;
}


-(void) addDataForDynamicChildsIteration:(id<KASeriazableObject>) Data
{
    
}

-(NSMutableArray<id<KASeriazableObjectTable>>*) getDataForDynamicChildIterationWithID:(NSString *)ID
{
    return nil;
}


-(NSString *) toFile:(KABasicFile *) File
{
    NSString *ReturnValue  = nil;
    KASerializableObjectToFile *Serializator = [[KASerializableObjectToFile alloc] initWithSerializableObject:self];
    
     NSError * Error = nil;
    [Serializator serializeToFile:File  WithError:Error];
    if(Error == nil)
    {
        ReturnValue = [File getFileCompleteName];
    }
    return ReturnValue;
}

-(NSString *) toString
{
    NSString * ReturnValue = nil;
    
    NSDictionary * AttributeAsDictionnary = [self toDictionnary];
    
    if(AttributeAsDictionnary != nil)
    {
        ReturnValue = [AttributeAsDictionnary toString];
    }
    return ReturnValue;
}

-(NSDictionary *) toDictionnary
{
    
    NSAssert(_ID != nil, @"%@ can't be nil for serialization",TYPE_ID);
    NSAssert(_label != nil, @"%@ can't be nil for serialization",LABEL);
    NSAssert(_objectFamilyName != nil, @"%@ can't be nil for serialization",GROUP_ID);
    NSAssert(_labelIdentifier != nil, @"%@ can't be nil for serialization",LABEL_IDENTIFIER);
    
    NSMutableDictionary *ReturnValue = nil;
    if(!self.isDeleted && self.isSerializable)
    {
        NSMutableArray *ChildList;
        ReturnValue = [[NSMutableDictionary alloc] init];
        
        [ReturnValue setObject:_ID  forKey:TYPE_ID];
        [ReturnValue setObject:_label  forKey:LABEL];
        [ReturnValue setObject:_objectFamilyName  forKey:GROUP_ID];
        [ReturnValue setObject:_labelIdentifier  forKey:LABEL_IDENTIFIER];
        
        for(int i = 0; (self.childs != nil) &&(i< [self.childs count]);i++)
        {
            id<KASeriazableObject> currentAttribute = [self.childs objectAtIndex:i];
            
            if(ChildList == nil)
            {
                ChildList = [[NSMutableArray alloc] init];
                [ReturnValue setObject:ChildList forKey:NODE_CHILDS_TAG];
            }
            id Current = [currentAttribute toDictionnary];
            if(Current != nil)
            {
                [ChildList addObject:Current];
            }
        }
    }
    
    return ReturnValue;
}


-(BOOL) isSerializable
{
    return YES;
}
-(void) setIsSerializable:(BOOL) isSerializable
{
    
}



-(BOOL) isDeleted
{
    return NO;
}
-(void) setIsDeleted:(BOOL) isDeleted
{
    
}



-(id<KASeriazableObject>) nextSibling
{
    id<KASeriazableObject> ReturnValue = nil;
    
    id<KASeriazableObject> Parent = [self parent];
    
    if(Parent != nil)
    {
        BOOL bFound = NO;
        id<KAIterator> It = [Parent getIterator];
        while([It hasNext] && !bFound)
        {
            id<KASeriazableObject> currentNode = [It next];
            bFound = ([self compareAttribute:currentNode] == NSOrderedSame);
            if( bFound )
            {
                ReturnValue = [It next];
            }
        }
    
    }
    return ReturnValue;
}

-(NSArray<id<KASeriazableObject>> *) childs
{
    NSMutableArray<id<KASeriazableObject>> * ReturnValue;
    
    id<KAIterator> Iterator = [self getIterator];
    while([Iterator hasNext])
    {
        while([Iterator hasNext])
        {
            id<KASeriazableObject> currentChild = [Iterator next];
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:currentChild ];
        }
    }
    return ReturnValue;
}




-(NSString *) label
{
    return _label;
}
-(void) setLabel:(NSString *) Label
{
    _label = Label;
}

-(NSString *) labelIdentifier
{
    return _labelIdentifier;
}
-(void) setLabelIdentifier:(NSString *) LabelIdentifier
{
    _labelIdentifier = LabelIdentifier;
}

-(NSString *) objectFamilyName
{
    return _objectFamilyName;
}

- (NSString *)ID { 
    return _ID;
}
- (void)setID:(NSString *)ID {
    _ID = ID;
}

-(void)setObjectFamilyName:(NSString *)objectFamilyName
{
    _objectFamilyName = objectFamilyName;
}

-(id<KASeriazableObject> ) nodeFromPathString:(NSString *) PathString
{
    id<KASeriazableObject> ReturnValue = nil;
    NSArray<id<KASeriazableObject>> *List = [self nodeListFromPathString:PathString];
    if(List != nil)
    {
        ReturnValue = [List objectAtIndex:([List count] - 1)];
    }
    return  ReturnValue;
}



-(NSArray<id<KASeriazableObject>> *) nodeListFromPathString:(NSString *) PathString
{
    NSMutableArray<id<KASeriazableObject>> *ReturnValue = nil;
    
    NSArray<NSString *> * IDList = [PathString nodeIDList];
    
    if([[IDList objectAtIndex:0] isEqual:self.ID])
    {
        id<KASeriazableObject> currentNode = self;
        ReturnValue = [[NSMutableArray alloc] init];
        [ReturnValue addObject:currentNode];
        
        for( int i = 1; (i< [IDList count])&& (currentNode != nil ); i++)
        {
            currentNode = [currentNode getChildwithTypeId:[IDList objectAtIndex:i]];
            [ReturnValue addObject:currentNode];
        }
    }
    return ReturnValue;
}

@end
