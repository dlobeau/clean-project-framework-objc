//
//  cleanProjectFramework.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 14/11/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for cleanProjectFramework.
FOUNDATION_EXPORT double cleanProjectFrameworkVersionNumber;

//! Project version string for cleanProjectFramework.
FOUNDATION_EXPORT const unsigned char cleanProjectFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <cleanProjectFramework/PublicHeader.h>


