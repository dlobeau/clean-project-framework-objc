//
//  TLUICollectionViewCellFacade.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "TLUICollectionViewCellFacade.h"
#import "KACell.h"

@interface TLUICollectionViewCellFacade()

@property (weak) id<KACell> interface;

@property (weak) id<KADelegatePushNextView> delegatePushNextView;

@end

@implementation TLUICollectionViewCellFacade

-(UIView *) view
{
    return self.contentView;
}


-(void) setContent
{
    
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return nil;
}

@end
