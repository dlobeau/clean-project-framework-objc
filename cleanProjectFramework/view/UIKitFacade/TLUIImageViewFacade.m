//
//  TLUIImageViewFacade.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "TLUIImageViewFacade.h"

@interface TLUIImageViewFacade()

@property (weak) id<KADelegatePushNextView> delegatePushNextView;
@property (weak) id<KAPresenter> interface;
@end

@implementation TLUIImageViewFacade

-(id) view
{
    return self;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return NO;
}

-(NSString *) widgetID
{
    return self.accessibilityIdentifier;
}

-(void) setContent
{
    
}


-(BOOL) validateUserChange
{
    return NO;
}

@end
