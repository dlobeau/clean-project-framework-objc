//
//  KAUITableView.h
//  kakebo
//
//  Created by Didier Lobeau on 19/04/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "TLUITableViewFacade.h"
#import "KAView.h"



@protocol  KAUIActiveCellDelegate;

@interface KAUITableView : TLUITableViewFacade<UITableViewDelegate,UITableViewDataSource>

-(void) registerSectionHeaderCellsWithIDList:(NSSet<NSString *> *)IDList;



@end
