//
//  KACreateUIWidgetCollection.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KACreateUIWidgetCollection.h"
#import "TLUICollectionViewFacade.h"
#import "KATable.h"

@implementation KACreateUIWidgetCollection

-(TLUICollectionViewFacade* ) createWithOwner:(id<KAView>)Owner WithWidgetType:(NSString *)WidgetType WithWidgetIdentifier:(NSString *)identifier  WithSender:(id<KATable>) Sender
{
    TLUICollectionViewFacade* ReturnValue = nil;
    
    ReturnValue =  (TLUICollectionViewFacade *)[super createWithOwner:Owner WithWidgetType:WidgetType WithWidgetIdentifier:identifier WithSender:Sender];
    
    NSAssert([ReturnValue isKindOfClass:TLUICollectionViewFacade.class], @"KACreateUIWidgetTable should create KAUITableView object");
    [ReturnValue registerCellsWithIDList:[self getCellsTypeListWithCollection:Sender]];
      
    return ReturnValue;
}

-(TLUICollectionViewFacade *) getChildWithID:(NSString *) ID WithWidgetParent:(id<KAView>) WidgetParent
{
    TLUICollectionViewFacade * ReturnValue= (TLUICollectionViewFacade*)[super getChildWithID:ID WithWidgetParent:WidgetParent];
    
     return ReturnValue;
    
}

-(NSSet<NSString *> *) getCellsTypeListWithCollection:(id<KATable>) CollectionPresenter
{
    NSMutableSet<NSString *> *ReturnValue = nil;
    
    NSArray<id<KASection>> *SectionList = (NSArray<id<KASection>> *)[(id<KASeriazableObjectTable>)CollectionPresenter childs];
    
    for(id<KASection>Section in SectionList)
    {
        if([Section conformsToProtocol:@protocol(KASection)])
        {
            NSArray<id<KACell>> *ListCell =  (NSArray<id<KACell>> *)[(id<KASeriazableObjectTable>)Section childs];
            for (id<KACell>Cell in ListCell)
            {
                if([Cell conformsToProtocol:@protocol(KACell)])
                {
                    if(ReturnValue == nil)
                    {
                        ReturnValue = [[NSMutableSet alloc]init];
                    }
                    [ReturnValue addObject:[Cell labelIdentifier]];
                }
            }
        }
    }
    
    return ReturnValue;
}
@end
