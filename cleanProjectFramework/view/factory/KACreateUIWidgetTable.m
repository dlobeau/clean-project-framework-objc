//
//  KACreateUIWidgetTable.m
//  kakebo
//
//  Created by Didier Lobeau on 20/09/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACreateUIWidgetTable.h"
#import "KAUITableView.h"
#import "KAView.h"
#import "KATable.h"

@implementation KACreateUIWidgetTable

-(KAUITableView* ) createWithOwner:(id<KAView>)Owner WithWidgetType:(NSString *)WidgetType WithWidgetIdentifier:(NSString *)identifier  WithSender:(id<KATable>) Sender
{
    KAUITableView* ReturnValue = nil;
    
    ReturnValue =  (KAUITableView *)[super createWithOwner:Owner WithWidgetType:WidgetType WithWidgetIdentifier:identifier WithSender:Sender];
    
    NSAssert([ReturnValue isKindOfClass:KAUITableView.class], @"KACreateUIWidgetTable should create KAUITableView object");
    ReturnValue.delegate = ReturnValue;
    ReturnValue.dataSource = ReturnValue;
  //  [(KAUITableView* )ReturnValue registerSectionHeaderCellsWithIDList:[Sender getHeaderTypeList]];
      
    return ReturnValue;
}

-(KAUITableView *) getChildWithID:(NSString *) ID WithWidgetParent:(id<KAView>) WidgetParent
{
    KAUITableView * ReturnValue= (KAUITableView*)[super getChildWithID:ID WithWidgetParent:WidgetParent];
    
    ReturnValue.delegate = ReturnValue;
    ReturnValue.dataSource = ReturnValue;
    
    return ReturnValue;
    
}

@end
