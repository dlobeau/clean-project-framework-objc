//
//  createUIWidgetStoroyboqrdFactory.m
//  kakebo
//
//  Created by Didier Lobeau on 07/10/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KACreateUIWidgetGenericFactory.h"
#import "KABasicFile.h"
#import "KAPresenter.h"
#import "KAView.h"
#import "UIView+KAKEBO.h"
#import "KAApplicationSerializableObjectImp.h"

@implementation KACreateUIWidgetGenericFactory

static NSDictionary<NSString *, NSDictionary<NSString*,NSString*> *> *WidgetTagList;

-(id<KAView> ) createWithOwner:(id<KAView>) Owner WithWidgetType:(NSString * ) WidgetType WithWidgetIdentifier:(NSString *) identifier  WithSender:(id<KAPresenter>) Sender
{

    id<KAView> ReturnValue = nil;
    
    NSString *Identifier = [Sender labelIdentifier];
    ReturnValue = [Owner.interface.viewFactoryDelegate getChildWithID:Identifier WithWidgetParent:Owner];
    
    if(ReturnValue == nil)
    {
       NSAssert([self isWidgetTypeValid:WidgetType], @"ID: %@ is undefined for widget: %@ ",WidgetType, [Sender attributeIdentity]);
        
        NSAssert([self isWidgetIdentifierValid:identifier WithWidgetID:WidgetType], @"identifier: %@ is undefined for widget: %@ ",identifier, [Sender attributeIdentity]);
        
        NSString *storyBoardId = [self getStoryBoardIDForWidget:WidgetType];
      
        if(storyBoardId != nil)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyBoardId bundle: nil];
            ReturnValue = [storyboard instantiateViewControllerWithIdentifier:identifier];
            ReturnValue.delegatePushNextView = [(id<KAView>)Owner delegatePushNextView];
        }
        else
        {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:identifier owner:Owner options:nil];
            
            if( topLevelObjects != nil )
            {
                ReturnValue = [topLevelObjects objectAtIndex:0];
                ReturnValue.delegatePushNextView = [(id<KAView>)Owner delegatePushNextView];
            }
        }
    }
    return ReturnValue;
    
}

-(NSDictionary *) getWidgetTagList
{
    if( WidgetTagList == nil)
    {
        id<KAApplication> Appli = [KAApplicationSerializableObjectImp instance];
        
        WidgetTagList = [Appli getWidgetTagList];
    }
    
    return WidgetTagList;
}

-(BOOL) isWidgetTypeValid:(NSString *) WidgetId
{
    BOOL bReturnValue = NO;
    
    NSDictionary *WidgetDictionnaryInfo = [[self getWidgetTagList] objectForKey:WidgetId];
    
    if(WidgetDictionnaryInfo != nil)
    {
        bReturnValue = YES;
    }
    
    return bReturnValue;
}

-(BOOL) isWidgetIdentifierValid:(NSString *) identifier WithWidgetID:(NSString *) WidgetID
{
    BOOL bReturnValue = NO;
    
    NSDictionary *WidgetDictionnaryInfo = [[self getWidgetTagList] objectForKey:WidgetID];
    
    if(WidgetDictionnaryInfo != nil)
    {
        NSDictionary *WidgetType = [WidgetDictionnaryInfo objectForKey:identifier];
        if(WidgetType != nil)
        {
            bReturnValue = YES;
        }
    }
    return bReturnValue;
}

-(NSString *) getStoryBoardIDForWidget:(NSString *) WidgetId
{
    NSString *ReturnValue = nil;
    
    NSDictionary *WidgetDictionnaryInfo = [[self getWidgetTagList] objectForKey:WidgetId];
    
    if(WidgetDictionnaryInfo != nil)
    {
            ReturnValue = [WidgetDictionnaryInfo objectForKey:@"storyBoard"];
    }

    return ReturnValue;
}

    -(id<KAView>) getChildWithID:(NSString *) ID WithWidgetParent:(id<KAView>) WidgetParent
    {
        id<KAView> returnValue = nil;
        UIView *ParentView = WidgetParent.view;
        
        returnValue = [ParentView getChildWithID:ID];
        
        return returnValue;
        
    }
    
@end
