//
//  KAASynchronousDataBindingDelegate.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@protocol KAASynchronousDataBindingDelegate 

-(void) didASynchronousDataBindingFinishedWithSender:(id) Sender;


@end

NS_ASSUME_NONNULL_END
