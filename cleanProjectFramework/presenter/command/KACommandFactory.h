//
//  KACommandFactory.h
//  taskList
//
//  Created by Didier Lobeau on 09/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@protocol KACommand;
@protocol KAPresenter;


@protocol KACommandFactory

-(id<KACommand>) createCommandOnLabelModificationWithNewLabel:(NSString *) NewLabel WithSender:(id<KAPresenter>) Sender;
-(id<KACommand>) createCommandOnPresenterModificationWithNewDate:(NSDate *) NewDate WithSender:(id<KAPresenter>) Sender;
-(id<KACommand>) createCommandOnPresenterModificationWithBoolean:(BOOL) BooleanValue WithSender:(id<KAPresenter>) Sender;
-(id<KACommand>) createCommandOnChildPresenterModification:(id<KAPresenter>) NewSelection WithSender:(id<KAPresenter>) Sender;
-(id<KACommand>) createCommandOnPresenterModificationWithPresenterWithNewValue:(id<KAPresenter>) PresenterWithNewValue WithSender:(id<KAPresenter>) Sender;

@end

NS_ASSUME_NONNULL_END
