//
//  KACommandChangeDataSelection.h
//  kakebo
//
//  Created by Didier Lobeau on 14/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KACommandGeneric.h"

@interface KACommandChangeData : KACommandGeneric

-(id) initWithOwner:(id<KAPresenter>)Owner WithNewChildSelected:(id<KAPresenter>) NewChildSelected;

@end
