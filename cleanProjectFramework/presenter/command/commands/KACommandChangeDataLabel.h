//
//  KACommandChangeDataLabel.h
//  kakebo
//
//  Created by Didier Lobeau on 14/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KACommandGeneric.h"



@interface KACommandChangeDataLabel : KACommandGeneric

-(id) initWithOwner:(id<KAPresenter>) Owner WithNewLabel:(NSString *) NewLabel;

@end
