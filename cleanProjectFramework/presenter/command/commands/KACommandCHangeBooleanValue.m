//
//  KACommandCHangeBooleanValue.m
//  taskList
//
//  Created by Didier Lobeau on 13/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACommandCHangeBooleanValue.h"
#import "KABoolean.h"
#import "KAApplicationSerializableObjectImp.h"
#import "KASerializeObjectFactory.h"

@interface KACommandCHangeBooleanValue()

@property  BOOL boolValue;;

@end

@implementation KACommandCHangeBooleanValue

-(id) initWithOwner:(id<KAPresenter>)Owner WithBooleanValue:(BOOL)BooleanValue
{
    self = [super initWithOwner:Owner];
    
    self.boolValue = BooleanValue;
    
    return self;
}

-(void) doCommand
{
    id<KABoolean> BooleanObject = [[[KAApplicationSerializableObjectImp instance] factory] createObjectFromFamilyType:@"bool"];
    [BooleanObject setValue:self.boolValue];
    [self.owner setData:BooleanObject];
}

@end
