//
//  KACommandChangeSelfData.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACommandChangeSelfData.h"
#import "KAPresenter.h"

@interface KACommandChangeSelfData()

@property id<KAPresenter> otherPresenter;

@end

@implementation KACommandChangeSelfData

-(id) initWithOwner:(id<KAPresenter>)Owner WithPresenter:(nonnull id<KAPresenter>)Presenter
{
    self = [super initWithOwner:Owner];
    self.otherPresenter = Presenter;
    return self;
}

-(void) doCommand
{
    [self.owner setData:self.otherPresenter.data];
}


@end
