//
//  KACommandChangeDataSelection.m
//  kakebo
//
//  Created by Didier Lobeau on 14/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KACommandChangeData.h"
#import "KADomain.h"
#import "KAPresenter.h"
#import "KASeriazableObjectTable.h"

@interface KACommandChangeData()

@property id<KAPresenter> childSelected;

@end


@implementation KACommandChangeData


-(id) initWithOwner:(id<KAPresenter>)Owner WithNewChildSelected:(id<KAPresenter>) NewChildSelected
{
    self = [super initWithOwner:Owner];
    self.childSelected = NewChildSelected;
    return self;
}



-(void) doCommand
{
    [self.owner.data modifyChildDomainWithId:self.childSelected.ID WithValue:self.childSelected.data];
  }

@end
