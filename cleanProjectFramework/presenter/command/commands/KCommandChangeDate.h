//
//  KCommandChangeDate.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACommandGeneric.h"

NS_ASSUME_NONNULL_BEGIN

@interface KCommandChangeDate : KACommandGeneric

-(id) initWithOwner:(id<KAPresenter>) Owner WithNewDate:(NSDate *) NewDate;

@end

NS_ASSUME_NONNULL_END
