//
//  KAEventValidateAction.m
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAEventValidateAction.h"
#import "KADomain.h"

@implementation KAEventValidateAction

-(void) doEventActionWithSender:(id<KAPresenter>) Sender
{
    id<KADomain> Data = [self getDestinationDataWithContext:Sender];
    [Data validate];
}

@end
