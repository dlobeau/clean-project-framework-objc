//
//  KAEventInterface.m
//  kakebo
//
//  Created by Didier Lobeau on 12/07/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KAEventInterface.h"
#import "KAPresenter.h"
#import "KASerializeObjectLink.h"
#import "KADomainLink.h"

@implementation KAEventInterface

static NSString * DESTINATION_TAG = @"destination";
static NSString * COMMAND_TAG = @"command";

+(NSString *) getTypeTag
{
    return @"event";
}

-(void) doEventActionWithSender:(id<KAPresenter>) Sender
{
}

-(id<KAPresenter>) getDestinationWithOwner:(id<KAPresenter>) Owner
{
    id<KAPresenter> ReturnValue = nil;
    
    ReturnValue =  (id<KAPresenter>)[self getChildwithTypeId:DESTINATION_TAG];
    if(ReturnValue != nil)
    {
        NSAssert([ReturnValue conformsToProtocol:@protocol(KAPresenter)], @"Error on Event:%@, destination should be of Window type",[self attributeIdentity]);
        ReturnValue.data = [self getDestinationDataWithContext:Owner];
    }
    return ReturnValue;
}

-(id<KADomain>) getDestinationDataWithContext:(id<KAPresenter>) Context
{
    
    id<KADomain> ReturnValue =nil;
    if([Context conformsToProtocol:@protocol(KAPresenter)])
    {
        ReturnValue = Context.data;
    }
   
    id<KADomain> Data = (id<KADomain>)[self getChildwithTypeId:@"data"];
    
    if(Data.isLink)
    {
        [(id<KADomainLink>)Data fetchSource];
    }
    
    if (Data != nil)
    {
        ReturnValue = Data;
    }
    return ReturnValue;
}


@end
