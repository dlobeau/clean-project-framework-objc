//
//  KAEventInterface.h
//  kakebo
//
//  Created by Didier Lobeau on 12/07/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectTableImp.h"
#import "KAEvent.h"

@interface KAEventInterface : KASeriazableObjectTableImp <KAEvent>

-(id<KADomain>) getDestinationDataWithContext:(id<KAPresenter>) Context;

@end
