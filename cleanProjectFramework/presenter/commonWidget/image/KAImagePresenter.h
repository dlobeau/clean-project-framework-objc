//
//  KAImagePresenter.h
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KAImage.h"

@interface KAImagePresenter : KAGenericPresenter<KAImage>

@end
