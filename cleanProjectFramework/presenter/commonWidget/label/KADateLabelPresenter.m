//
//  KADateLabelPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KADateLabelPresenter.h"
#import "KADate.h"

@implementation KADateLabelPresenter

+(NSString *) getTypeTag
{
    return @"dateLabel";
}

-(NSString*) getText
{
    
    id<KADate> DAte = ( id<KADate>) [self data];
   
    return [DAte completeString];
    
}

@end


