//
//  KASectionPresenter.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KASection.h"
NS_ASSUME_NONNULL_BEGIN

@interface KASectionPresenter : KAGenericPresenter<KASection>

@end

NS_ASSUME_NONNULL_END
