//
//  KATablePresenter.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KATable.h"
NS_ASSUME_NONNULL_BEGIN

@interface KATablePresenter : KAGenericPresenter<KATable>

@end

NS_ASSUME_NONNULL_END
