//
//  KASection.h
//  taskList
//
//  Created by Didier Lobeau on 28/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"
#import "KACell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol KACell;

@protocol KASection <KAPresenter>

-(NSArray<id<KACell>> *) cells;
-(id<KACell>) cellAtIndex:(NSInteger) Index;
@optional

-(id<KAPresenter>) header;
-(id<KAPresenter>) footer;

@end

NS_ASSUME_NONNULL_END
