//
//  KACellPresenter.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KACell.h"
NS_ASSUME_NONNULL_BEGIN

@interface KACellPresenter : KAGenericPresenter<KACell>

@end

NS_ASSUME_NONNULL_END
