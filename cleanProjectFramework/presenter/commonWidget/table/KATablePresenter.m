//
//  KATablePresenter.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 20/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KATablePresenter.h"

@implementation KATablePresenter

+(NSString *) getTypeTag
{
    return @"table";
}

-(NSArray<id<KASection>> *) sections
{
    NSMutableArray<id<KASection>> *ReturnValue = nil;
    
    NSArray<id<KASection>> *allCHilds = (NSArray<id<KASection>> *)[self childPresenter];
    
    for(int i = 0; i< allCHilds.count; i++)
    {
        id<KASection> CurrentSection = [allCHilds objectAtIndex:i];
        if([CurrentSection conformsToProtocol: @protocol(KASection) ])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:CurrentSection];
        }
        
    }
    return ReturnValue;
}

-(NSArray<id<KACell>> *) allCells
{
    NSMutableArray<id<KACell>> *ReturnValue = nil;
    NSArray<id<KASection>> *allSection = (NSArray<id<KASection>> *)[self sections];
    
    for(int i = 0; i< allSection.count; i++)
    {
        NSArray<id<KACell>> *  CurrentCellList = [[allSection objectAtIndex:i] cells];
        if(ReturnValue == nil)
        {
            ReturnValue = [[NSMutableArray alloc] init];
            
        }
        [ReturnValue addObjectsFromArray:CurrentCellList];
        
    }
    return ReturnValue;
}

-(id<KASection>) sectionAtIndex:(NSInteger) Index
{
    NSArray<id<KASection>> *Sections = self.sections;
    NSAssert(Index< Sections.count,@"index: %ld can't be bigger than Section list: %ld",Index,Sections.count);
    
    return [Sections objectAtIndex:Index];
}




-(NSSet<NSString *> *) getHeaderTypeList
{
    NSMutableSet<NSString *> *ReturnValue = nil;
    
    NSArray<id<KASection> > *SectionList = [self sections];
    
    for(int i = 0; i < [SectionList count]; i++)
    {
        if(ReturnValue == nil)
        {
            ReturnValue = [[NSMutableSet alloc]init];
        }
        id<KASection> CurrentSection = [SectionList objectAtIndex:i];
        if(CurrentSection.header != nil)
        {
            [ReturnValue addObject:[CurrentSection.header labelIdentifier]];
        }
    }
    return ReturnValue;
}


@end
