//
//  TLButtonPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLButtonPresenter.h"
#import "TLButtonLabel.h"

@interface TLButtonPresenter()

@property id<TLButtonLabel> buttonLabel;

@end

@implementation TLButtonPresenter

@synthesize buttonLabel = _buttonLabel;

-(id<TLButtonLabel>) buttonLabel
{
    if(_buttonLabel == nil)
    {
        _buttonLabel = (id<TLButtonLabel>)[self getChildwithIdentifier:@"buttonLabel"];
    }
    return _buttonLabel;
}
-(void) setButtonLabel:(id<TLButtonLabel>)buttonLabel
{
    _buttonLabel = buttonLabel;
}

-(NSString *) getText
{
    NSString * ReturnValue = [super getText];
    if(self.buttonLabel != nil)
    {
        ReturnValue = self.buttonLabel.getText;
    }
    return ReturnValue;
}

@end
