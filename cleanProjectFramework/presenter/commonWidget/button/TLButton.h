//
//  TLButton.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TLButtonLabel;

@protocol TLButton <KAPresenter>

-(id<TLButtonLabel>) buttonLabel;

@end

NS_ASSUME_NONNULL_END
