//
//  TLNewWindowButton.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLButtonPresenter.h"
#import "TLButton.h"
#import "KAPresenterConformToDomain.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLNewWindowButtonPresenter : TLButtonPresenter<TLButton>

@end

@interface TLNewWindowButtonPresenterConformToDomain : NSObject<KAPresenterConformToDomain>

@end
NS_ASSUME_NONNULL_END
