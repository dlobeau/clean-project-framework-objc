//
//  KADateAttribute.h
//  kakebo
//
//  Created by Didier Lobeau on 11/08/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAGenericDomain.h"
#import "KADate.h"


@protocol KAYear;
@protocol KACalendar;
@interface KADateAttribute : KAGenericDomain <KADate>

    -(id) initWithStandardDateObject:(NSDate *) DateObject WithTypeId:(NSString *)TypeId;

    -(id) initWithDateString:(NSString *) DateString WithTypeId:(NSString *)TypeId;

-(id) NULL_DATE;

@end
