//
//  KANullDomain.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 24/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KAGenericDomain.h"

NS_ASSUME_NONNULL_BEGIN

@interface KANullDomain : KAGenericDomain

+(id<KADomain>) nullDomainObject;

@end

NS_ASSUME_NONNULL_END
