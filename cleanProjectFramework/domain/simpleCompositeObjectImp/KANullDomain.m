//
//  KANullDomain.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 24/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KANullDomain.h"

@implementation KANullDomain

static NSString *NULL_DOMAIN = @"NULL_DOMAIN";
static id<KADomain> nullObjectSingleton;


+(id<KADomain>) nullDomainObject
{
    if (nullObjectSingleton == nil)
    {
        nullObjectSingleton = [[KANullDomain alloc] init];
    }
    return nullObjectSingleton;
}

+(NSString *) getTypeTag
{
    return NULL_DOMAIN;
}

-(id) init
{
    self = [super initWithLabel:NULL_DOMAIN WithLabelIdentifier:NULL_DOMAIN WithObjectFamilyName:NULL_DOMAIN WithID:NULL_DOMAIN];
    
    return self;
}

-(BOOL) isNull
{
    return YES;
}

@end
