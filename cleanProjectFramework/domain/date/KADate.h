//
//  KADate.h
//  kakebo
//
//  Created by Didier Lobeau on 11/08/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADomain.h"

@protocol KACalendar;

@protocol KADate <KADomain>

-(NSString *) dayInMonth;

-(NSString *) completeString;

-(NSDate *) getDateObject;

-(id<KACalendar>) calendar;
-(void) setCalendar:(id<KACalendar>) Calendar;

@end
