//
//  KAQueryOperatorNoAction.h
//  taskList
//
//  Created by Didier Lobeau on 27/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KASeriazableObjectTableImp.h"
#import "KAQueryBusinessUseCase.h"
NS_ASSUME_NONNULL_BEGIN

@interface KAQueryBusinessUseCaseNoAction : KASeriazableObjectTableImp<KAQueryBusinessUseCase>

@end

NS_ASSUME_NONNULL_END
