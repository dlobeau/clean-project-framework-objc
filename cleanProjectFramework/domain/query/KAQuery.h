//
//  KAQuery.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol KACriteria;
@protocol KADomain;

@protocol KAQuery 

-(NSArray<id<KACriteria>>*) criterias;
-(id<KADomain>) repository;

-(void) execute;

-(id<KADomain>) result;




@end
