//
//  KACriteria.h
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//



@protocol KAQueryBusinessUseCase;
@protocol KADomain;

@protocol KACriteria 

-(id<KAQueryBusinessUseCase>) queryBusinessUseCase;
-(id<KADomain>) field;
-(id<KADomain>) value;
-(void) setValue:(id<KADomain>) Value;


@end
